var numPlayers = 6
var unitsPerPlayer =  numPlayers==10 ? 2 :
                      numPlayers==9  ? 2 :
                      numPlayers==8  ? 3 :
                      numPlayers==7  ? 3 :
                      numPlayers==6  ? 3 :
                      numPlayers==5  ? 4 :
                      numPlayers==4  ? 5 :
                      numPlayers==3  ? 7 :
                      numPlayers==2  ? 10 : 0
console.log('unitsPerPlayer =',unitsPerPlayer)
var tableWidth = numPlayers<7 ? 3500 : 5000
var numBottomRowPlayers = Math.round(numPlayers/2)
var numTopRowPlayers = numPlayers-numBottomRowPlayers
var topRowOrigins = []
var bottomRowOrigins = []
for(i=0; i<numTopRowPlayers; i++) {
  var alpha = (i+1)/(numTopRowPlayers+1)
  var x = -tableWidth*alpha+tableWidth*(1-alpha)
  topRowOrigins.push([x,-1850])
}
for(i=0; i<numBottomRowPlayers; i++) {
  var alpha = (i+1)/(numBottomRowPlayers+1)
  var x = -tableWidth*alpha+tableWidth*(1-alpha)
  bottomRowOrigins.push([x,1850])
}
origins = topRowOrigins.concat(bottomRowOrigins)

shuffle = v => {
  for(
    var j, x, i = v.length; i; j = parseInt(Math.random() * i),
    x = v[--i],
    v[i] = v[j],
    v[j] = x
  );
  return v;
};

startingWars = {
  commander: [
    ['a4', 'a5', 'a6', 'b3', 'b4', 'b5', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'd1', 'd2', 'd3', 'd5', 'd6', 'd7', 'd8', 'e4', 'd7'],
    ['a4', 'a5', 'a6', 'b3', 'b4', 'b5', 'c4', 'c5', 'c6', 'c7', 'd5', 'd6', 'd7', 'd8', 'e4', 'e6', 'e7', 'e8', 'f5', 'f6', 'b3'],
    ['a6', 'b5', 'c5', 'c6', 'c7', 'd5', 'd6', 'd7', 'd8', 'e6', 'e7', 'e8', 'f5', 'f6', 'g6', 'g7', 'i6', 'i7', 'j6', 'j7', 'c5'],
    ['d7', 'd8', 'e6', 'e7', 'e8', 'f4', 'f5', 'f6', 'g4', 'g6', 'g7', 'h3', 'h4', 'i2', 'i4', 'i6', 'i7', 'j3', 'j6', 'j7', 'f4'],
    ['f3', 'f4', 'g1', 'g2', 'g3', 'g4', 'g6', 'g7', 'h1', 'h2', 'h3', 'h4', 'i1', 'i2', 'i4', 'i6', 'i7', 'j3', 'j6', 'j7', 'f2'],
    ['d1', 'e1', 'e2', 'f1', 'f2', 'f3', 'g1', 'g2', 'g3', 'h1', 'h2', 'h3', 'h4', 'i1', 'i2', 'i4', 'i6', 'j3', 'j6', 'j7', 'f4'],
    ['b3', 'c2', 'c3', 'd1', 'd2', 'd3', 'e1', 'e2', 'e3', 'f1', 'f2', 'g1', 'g2', 'g3', 'h1', 'h2', 'h3', 'i1', 'i2', 'j3', 'h4'],
    ['a4', 'a5', 'a6', 'b3', 'b4', 'b5', 'c2', 'c3', 'c4', 'c5', 'd1', 'd2', 'd3', 'e1', 'e2', 'e3', 'e4', 'f1', 'f2', 'g1', 'c6'],
    ['d2', 'd3', 'd5', 'd6', 'e2', 'e3', 'e4', 'e6', 'f2', 'f3', 'f4', 'f5', 'f6', 'g2', 'g3', 'g4', 'g6', 'h2', 'h3', 'h4', 'h4'],
    ['d1', 'd2', 'd3', 'd4', 'd5', 'd6', 'd7', 'd8', 'e2', 'e3', 'e4', 'e6', 'e7', 'e8', 'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'c7'], 
  ],
  army: [
    ['c2', 'c5', 'd1', 'd7', 'd8', 'e1', 'e3', 'e4', 'e7', 'e8', 'f3', 'f4', 'f5', 'g1', 'h3', 'h4', 'i1', 'i7', 'j3', 'j6', 'a5'],
    ['a4', 'a5', 'b3', 'b4', 'c2', 'c3', 'c5', 'd1', 'e1', 'e3', 'e4', 'f3', 'f4', 'f5', 'g1', 'h3', 'h4', 'i1', 'j4', 'j6', 'd8'],
    ['a4', 'a5', 'b3', 'b4', 'c2', 'c3', 'c5', 'd1', 'd7', 'd8', 'e1', 'e3', 'e4', 'e7', 'e8', 'f3', 'f4', 'f5', 'g1', 'h3', 'i7'],
    ['a4', 'a5', 'b3', 'b4', 'c3', 'c5', 'd7', 'd8', 'e3', 'e4', 'e7', 'e8', 'f3', 'f4', 'f5', 'h3', 'h4', 'i7', 'j3', 'j6', 'f2'],
    ['a4', 'a5', 'c3', 'c5', 'd1', 'd7', 'd8', 'e1', 'e3', 'e4', 'e7', 'e8', 'f3', 'f4', 'f5', 'g1', 'h3', 'h4', 'i1', 'i7', 'b4'],
    ['a4', 'a5', 'b3', 'c2', 'c3', 'c5', 'd7', 'd8', 'e1', 'e3', 'f3', 'f4', 'f5', 'g1', 'h3', 'i1', 'i7', 'j3', 'j6', 'g4'],
    ['a4', 'a5', 'b3', 'b4', 'c2', 'c3', 'd1', 'd7', 'd8', 'e1', 'e3', 'e4', 'e7', 'e8', 'f3', 'f4', 'f5', 'h3', 'h4', 'j6', 'c6'],
    ['a4', 'a5', 'b3', 'b4', 'c2', 'c3', 'c5', 'd1', 'd7', 'd8', 'e1', 'e7', 'e8', 'g1', 'h3', 'h4', 'i1', 'i7', 'j3', 'j6', 'e4'],
    ['a4', 'b3', 'b4', 'c3', 'd1', 'd7', 'd8', 'e1', 'e7', 'e8', 'f3', 'f4', 'g1', 'h3', 'h4', 'i1', 'i7', 'j3', 'c6'],
    ['a5', 'b4', 'c3', 'c5', 'd1', 'd7', 'd8', 'e3', 'e4', 'e7', 'e8', 'f3', 'f4', 'f5', 'g1', 'h3', 'h4', 'i7', 'i3', 'j6', 'c3'],
  ],
  navy: [
    ['a6', 'b5', 'c4', 'c7', 'd2', 'd3', 'd5', 'e2', 'f1', 'f2', 'g2', 'g3', 'g6', 'g7', 'h2', 'h6', 'i2', 'i4', 'i6', 'j7', 'c7'],
    ['a6', 'b5', 'c4', 'c5', 'c6', 'c7', 'd2', 'd3', 'd5', 'd6', 'e2', 'e6', 'f6', 'g3', 'g4', 'g6', 'g7', 'i4', 'i6', 'j7', 'h2'],
    ['a6', 'b5', 'c4', 'c6', 'c7', 'd2', 'd3', 'd4', 'd5', 'd6', 'e2', 'e6', 'f1', 'f2', 'f6', 'g2', 'g3', 'g4', 'h1', 'h2', 'i2', 'f6'],
    ['c4', 'd2', 'd3', 'd5', 'e2', 'e6', 'f1', 'f2', 'f6', 'g2', 'g3', 'g4', 'g6', 'g7', 'h1', 'h2', 'i2', 'i4', 'i6', 'j7', 'c6'],
    ['b5', 'c4', 'c6', 'c7', 'd2', 'd3', 'd4', 'd6', 'e2', 'e6', 'f2', 'f6', 'g2', 'g3', 'g4', 'g6', 'h2', 'i2', 'i4', 'i6', 'g4'],
    ['a6', 'b5', 'c4', 'c6', 'c7', 'd5', 'd6', 'e6', 'f6', 'g2', 'g3', 'g4', 'g6', 'g7', 'h1', 'h2', 'i2', 'i4', 'i6', 'j7', 'e2'],
    ['a6', 'b5', 'c4', 'c6', 'c7', 'd2', 'd3', 'd5', 'd6', 'e2', 'e6', 'f1', 'f2', 'f6', 'g2', 'g3', 'g4', 'g6', 'g7', 'h2', 'i7'],
    ['a6', 'b5', 'c6', 'c7', 'd3', 'd5', 'd6', 'e2', 'e6', 'f1',' f6', 'g2', 'g3', 'g4', 'g6', 'g7', 'h1', 'i2', 'i6', 'j7', 'g6'],
    ['a6', 'c6', 'c7', 'd2', 'd3', 'd6', 'e2', 'e6', 'f1', 'f2', 'f6', 'g2', 'g3', 'g6', 'g7', 'h1', 'h2', 'i2', 'i6', 'j6', 'e3'],
    ['a6', 'b5', 'c4', 'c6', 'c7', 'd2', 'd3', 'd5', 'd6', 'e2', 'e6', 'g2', 'g3', 'g4', 'g6', 'g7', 'i2', 'i4', 'i6', 'j7', 'e2'],
  ],
  merchant: [
    ['a4', 'a5', 'a6', 'd1', 'd8', 'e1', 'e3', 'e4', 'e8', 'f1', 'f3', 'f4', 'f5', 'g1', 'g3', 'h1', 'i1', 'j3', 'j6', 'j7', 'f2'],
    ['a4', 'a6', 'b3', 'c2', 'c6', 'd5', 'd7', 'e1', 'e3', 'e6', 'e8', 'f5', 'g1', 'g4', 'g6', 'h3', 'i1', 'i4', 'i7', 'j6'],
    ['c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'd2', 'd7', 'd8', 'e2', 'e7', 'e8', 'f2', 'g2', 'g7', 'h2', 'i2', 'i4', 'i6', 'i7', 'c5'],
    ['a6', 'b5', 'c4', 'c5', 'd1', 'd5', 'd8', 'e1', 'e4', 'e8', 'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'g4', 'h4', 'i4', 'j3', 'e3'],
    ['a4', 'a6', 'c2', 'c4', 'c6', 'e2', 'e4', 'e6', 'e8', 'g2', 'g4', 'g6', 'g7', 'i2', 'i4', 'i6', 'i7', 'j3', 'j6', 'j7', 'e8'],
    ['a4', 'a5', 'c2', 'c3', 'c6', 'c7', 'e1', 'e2', 'e7', 'e8', 'f4', 'f5', 'g1', 'g2', 'h3', 'h4', 'i1', 'i2', 'j6', 'j7', 'g3'],
    ['a4', 'a5', 'b3', 'b5', 'c2', 'c4', 'c6', 'c7', 'd1', 'd3', 'd5', 'e4', 'e8', 'f5', 'f1', 'i1', 'i2', 'i4', 'j3', 'j6', 'j7', 'c3'],
    ['a4', 'a5', 'a6', 'b3', 'c2', 'd1', 'd8', 'e1', 'e8', 'f1', 'g1', 'g7', 'h1', 'i1', 'i2', 'i3', 'i7', 'j3', 'j6', 'j7', 'f8'],
    ['d1', 'd2', 'd3', 'd5', 'd6', 'd7', 'd8', 'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'h1', 'h2', 'h3', 'h4', 'j3', 'j6', 'j7', 'c5'],
    ['a4', 'b4', 'c4', 'c7', 'd1', 'd7', 'e1', 'e4', 'e7', 'f1', 'f4', 'g1', 'g4', 'g7', 'h1', 'h4', 'i1' ,'i4', 'i7', 'j7', 'd5'],
  ]
}

const describeRow = function(file,x,y,type,n,length) {
  let descriptions = []
  for(i=0; i<n; i++) {
    var alpha = 0
    if(n>1) alpha = i/(n-1)
    var myX = (x-0.5*length)*(1-alpha) + (x+0.5*length)*alpha
    description = client.describe({file:file,x:myX,y:y,type:type})
    descriptions.push(description)
  }
  return(descriptions)
}

const describePortfolio = (x,y,color) => {
  let descriptions = []
  let sgn = Math.sign(y)
  angle = 0
  offset = 0
  if(sgn==-1) {
    angle = 180
    offset = 1
  }
  descriptions.push(client.describe({file:'board/portfolio',x:x,y:y-15*offset,type:'portfolio',rotation:angle}))
  descriptions = descriptions.concat([
    client.describe({file:'contract/merchant',   x:x-500,y:y-sgn*460, type:'card'}),
    client.describe({file:'contract/commander',  x:x-200,y:y-sgn*460, type:'card'}),
    client.describe({file:'contract/navy',      x:x+200,y:y-sgn*460, type:'card'}),
    client.describe({file:'contract/army',       x:x+500,y:y-sgn*460, type:'card'}),
  ])
  descriptions = descriptions.concat([client.describe({file:'unit/'+color+'-merchant',x:x-150,y:y-sgn*150,type:'bit'})])
  descriptions = descriptions.concat([client.describe({file:'good/1',x:x+0,y:y-sgn*150, type:'bit'})])
  descriptions = descriptions.concat([client.describe({file:'unit/'+color+'-merchant',x:x+150,y:y-sgn*150, type:'bit'})])
  descriptions = descriptions.concat(describeRow('unit/'+color,x+0,y-sgn*10,'bit',n=unitsPerPlayer,length=100*unitsPerPlayer))
  descriptions = descriptions.concat(describeRow('gold/1',x,y+sgn*200,'card',n=6,length=1000))
  descriptions = descriptions.concat(describeRow('gold/1',x-200,y+sgn*450,'card',n=4,length=600))
  descriptions = descriptions.concat(describeRow('gold/5',x+400,y+sgn*450,'card',n=2,length=200))
  return(descriptions)
}

const describeBankComponents = (x,y) => {
  var descriptions = [
    client.describe({file:'gold/1',  x:x-400,y:y-250,type:'card',clones:10}),
    client.describe({file:'gold/1',  x:x-200,y:y-250,type:'card',clones:10}),
    client.describe({file:'gold/1',  x:x-000,y:y-250,type:'card',clones:10}),
    client.describe({file:'gold/1',  x:x+200,y:y-250,type:'card',clones:10}),
    client.describe({file:'gold/1',  x:x+400,y:y-250,type:'card',clones:10}),
    client.describe({file:'gold/5',  x:x-200,y:y+50 ,type:'card',clones:20}),
    client.describe({file:'unit/war',x:x+0  ,y:y+50 ,type:'bit',clones:50}),
    client.describe({file:'good/5',  x:x+200,y:y+50 ,type:'bit',clones:20}),
    client.describe({file:'good/1',  x:x-400,y:y+300,type:'bit',clones:10}),
    client.describe({file:'good/1',  x:x-200,y:y+300,type:'bit',clones:10}),
    client.describe({file:'good/1',  x:x-000,y:y+300,type:'bit',clones:10}),
    client.describe({file:'good/1',  x:x+200,y:y+300,type:'bit',clones:10}),
    client.describe({file:'good/1',  x:x+400,y:y+300,type:'bit',clones:10}),
  ]
  return(descriptions)
}

points = {
  a5: {x:0,y:0}
}

const setup = message => {
  var contractTypes = ['merchant','commander','army','navy']
  contractTypes.map(type => { 
    startingWars[type] = shuffle(startingWars[type])
  })
  var playerColors = ['purple','yellow','grey','green','brown','blue','orange']
  playerColors = shuffle(playerColors)
  playerColors = playerColors.concat(['red','pink','white'])
  let descriptions = []
  descriptions = descriptions.concat([
    client.describe({file:'board/map',x:0,y:0,type:'board'}),
    client.describe({file:'board/bank',x:2000,y:0,type:'board'}),
    client.describe({file:'board/market',x:-2000,y:0,type:'board'})
  ])
  startingContract = contractTypes[Math.floor(Math.random()*4)]
  descriptions = descriptions.concat(
    client.describe({
      file:`contract/${startingContract}`,
      x:-2000,y:0,type:'card',
      details:startingWars[startingContract].pop().join()}))
  descriptions = descriptions.concat(describeBankComponents(2000,0))
  origins.map((origin,i) => { 
    x = origin[0]
    y = origin[1]
    const portfolio = describePortfolio(x,y,playerColors[i]) 
    descriptions = descriptions.concat(portfolio)
  })
  compareFunction = (a,b) => {
    aLayer = 5
    bLayer = 5
    if(a.file=='board/map') { aLayer = 0 }
    if(b.file=='board/map') { bLayer = 0 }
    if(a.file=='board/bank') { aLayer = 0 }
    if(b.file=='board/bank') { bLayer = 0 }
    if(a.file=='board/market') { aLayer = 0 }
    if(b.file=='board/market') { bLayer = 0 }
    if(a.file.substring(0,4)=='good') { aLayer = 1 }
    if(b.file.substring(0,4)=='good') { bLayer = 1 }
    if(a.file.substring(0,4)=='unit') { aLayer = 2 }
    if(b.file.substring(0,4)=='unit') { bLayer = 2 }
    if(a.file.substring(0,8)=='contract') { aLayer = 3 }
    if(b.file.substring(0,8)=='contract') { bLayer = 3 }
    if(a.file.substring(0,4)=='gold') { aLayer = 4 }
    if(b.file.substring(0,4)=='gold') { bLayer = 4 }
    if(a.file=='board/portfolio') { aLayer = 5 }
    if(b.file=='board/portfolio') { bLayer = 5 }
    return aLayer-bLayer
  }
  descriptions.sort(compareFunction)
  console.log(descriptions)
  client.start(descriptions,message.state)
}
