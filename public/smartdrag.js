Snap.plugin( function( Snap, Element, Paper, global ) {

  let shiftDown = false

  const mouseClick = event => {
    // console.log("mouseClick")
  }

  const dragStart = function(x,y,event) {
    const move = event.button===0 && !shiftDown && ['card','bit'].includes(this.data('type'))
    const flip = event.button===0 && shiftDown && this.data('twoSided')
    console.log(this.data('type'))
    details = this.data('details')
    if(details && this.data('side')!='back') {
      console.log(details)
    }
    if(move) {
      this.data('ot', this.transform().local)
      this.data('dragging', true)
      this.data('rotating', false)
    } else if(flip) {
      flipComponent(this)
      this.data('moved',true)
    }
  }

  const dragMove = function(dx, dy, event, x, y) {
    if(this.data('dragging')) {
      this.data('moved',true)
      if( this.data('type')=='bit' || this.data('type')=='card' ) {
        const snapInvMatrix = this.transform().diffMatrix.invert()
        snapInvMatrix.e = 0
        snapInvMatrix.f = 0
        const tdx = snapInvMatrix.x( dx,dy )
        const tdy = snapInvMatrix.y( dx,dy )
        this.transform(`t${tdx},${tdy}${this.data('ot')}`)
      }
    } 
  }

  const dragEnd = function() {
    this.data('dragging', false)
  }

  this.onkeydown = event => {
    if(event.key==="Shift") shiftDown = true
  }

  this.onkeyup = event => {
    if(event.key==="Shift") shiftDown = false
  }

  Element.prototype.smartdrag = function() {
    this.drag(dragMove, dragStart, dragEnd)
    this.mousedown(mouseClick)
    return this
  }

})
